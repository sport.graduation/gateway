package com.powerfit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class JwtValidationFilter extends AbstractGatewayFilterFactory<JwtValidationFilter.Config> {

    @Qualifier("excludedUrls")
    ArrayList<String> excludedUrls = new ArrayList<String>(
        Arrays.asList("/api/v1/accounts/player_signup",
                      "/api/v1/accounts/coach_signup",
                      "/api/v1/accounts/verify",
                     "login"));
    @Autowired
    private final WebClient.Builder webClientBuilder;

    public JwtValidationFilter(WebClient.Builder webClientBuilder) {
        super(Config.class);
        this.webClientBuilder = webClientBuilder;
    }

    @Autowired
    private ObjectMapper objectMapper;


    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();
            log.info("URL is - " + request.getURI().getPath());
            String bearerToken = request.getHeaders().getFirst("Authorization");
            log.info("Bearer Token: " + bearerToken);

            if (isSecured.test(request)) {
                return webClientBuilder.build().post()
                                       .uri("lb://ACCOUNTS-MANAGEMENT/api/v1/accounts/validateToken")
                                       .header("Authorization", bearerToken)
                                       .retrieve().bodyToMono(ConnValidationResponse.class)
                                       .map(response -> {
                                           exchange.getRequest().mutate().header("username", response.getUsername());
                                           return exchange;
                                       }).flatMap(chain::filter).onErrorResume(error -> {
                        log.info("Error Happened");
                        HttpStatus errorCode = null;
                        String errorMsg = "";

                        if (error instanceof WebClientResponseException) {
                            WebClientResponseException webClientResponseException = (WebClientResponseException) error;
                            errorCode = webClientResponseException.getStatusCode();
                            errorMsg = webClientResponseException.getStatusText();


                        } else {
                            errorCode = HttpStatus.BAD_GATEWAY;
                            errorMsg = HttpStatus.BAD_GATEWAY.getReasonPhrase();
                        }
                            //AuthorizationFilter.AUTH_FAILED_CODE
                        return onError(exchange, String.valueOf(errorCode.value()) ,errorMsg, "JWT Authentication Failed", errorCode);
                    });
            }

            return chain.filter(exchange);
        };
    }

    public Predicate<ServerHttpRequest> isSecured = request -> excludedUrls.stream().noneMatch(
        uri -> request.getURI().getPath().contains(uri));


    private Mono<Void> onError(ServerWebExchange exchange, String errCode, String err, String errDetails, HttpStatus httpStatus) {
        DataBufferFactory dataBufferFactory = exchange.getResponse().bufferFactory();
//        ObjectMapper objMapper = new ObjectMapper();
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(httpStatus);
        try {
            response.getHeaders().add("Content-Type", "application/json");
            ExceptionResponse data = new ExceptionResponse(errCode, err, errDetails, null, new Date());
            byte[] byteData = objectMapper.writeValueAsBytes(data);
            return response.writeWith(Mono.just(byteData).map(t -> dataBufferFactory.wrap(t)));

        } catch (JsonProcessingException e) {
            e.printStackTrace();

        }
        return response.setComplete();
    }


    @NoArgsConstructor
    public static class Config {
    }
}


