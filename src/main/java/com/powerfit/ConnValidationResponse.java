package com.powerfit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;

@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ConnValidationResponse {
    private boolean isAuthenticated;
    private String username;
}
