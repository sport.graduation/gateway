package com.powerfit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEurekaClient
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder, JwtValidationFilter filter) {
        return builder.routes()
                      .route("#0",
                             r -> r.path("/login")
                                   .uri("lb://ACCOUNTS-MANAGEMENT"))
                      .route("#1",
                             r -> r.path("/api/v1/accounts/**")
                                   .filters(f ->
                                               f.filter(filter.apply(new JwtValidationFilter.Config())))
                                   .uri("lb://ACCOUNTS-MANAGEMENT"))
                      .route("#2",
                             r -> r.path("/api/v1/instructions/**")
                                   .filters(f ->
                                                f.filter(filter.apply(new JwtValidationFilter.Config())))
                                   .uri("lb://INSTRUCTION"))
                      .route("#3",
                             r -> r.path("/api/v1/info/**")
                                   .filters(f ->
                                                f.filter(filter.apply(new JwtValidationFilter.Config())))
                                   .uri("lb://INFORMATION"))
                      .route("#4",
                             r -> r.path("/api/v1/getdata/**")
                                   .filters(f ->
                                                f.filter(filter.apply(new JwtValidationFilter.Config())))
                                   .uri("lb://GETDATA"))
                      .route("#5",
                             r -> r.path("/api/v1/display/**")
                                   .filters(f ->
                                                f.filter(filter.apply(new JwtValidationFilter.Config())))
                                   .uri("lb://DISPLAY-ANALYZE"))
                      .route("#6",
                             r -> r.path("/api/v1/evaluation/**")
                                   .filters(f ->
                                                f.filter(filter.apply(new JwtValidationFilter.Config())))
                                   .uri("lb://PRACTICE-EVALUATION"))
                      .route("#7",
                             r -> r.path("/api/v1/practice/**")
                                   .filters(f ->
                                                f.filter(filter.apply(new JwtValidationFilter.Config())))
                                   .uri("lb://PRACTICE-TIME"))
                      .route("#8",
                             r -> r.path("/api/v1/notifications/**")
                                   .filters(f ->
                                                f.filter(filter.apply(new JwtValidationFilter.Config())))
                                   .uri("lb://NOTIFICATIONS"))
                      .build();
    }

}
