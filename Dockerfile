FROM openjdk:11.0.16-oracle

WORKDIR /app

EXPOSE 10000

COPY target/*.jar gateway.jar

ENTRYPOINT [ "java","-jar","./gateway.jar"]

